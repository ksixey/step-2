let gulp = require('gulp');
let sass = require('gulp-sass');
let sync = require('browser-sync');
let concat = require('gulp-concat');

gulp.task('scss', function ( ) {
    return gulp.src('src/scss/**/*.scss')
        .pipe(sass({outputStyle:'compressed'}))
        .pipe(gulp.dest('src/css'))
        .pipe(sync.reload({stream:true}))
});



gulp.task('html',function () {
   return  gulp.src('src/*.html')
       .pipe(sync.reload({stream:true}))
});

gulp.task('js', function () {
    return gulp.src('src/main/*.js')
        .pipe(sync.reload({stream:true}))
});

gulp.task('watch', function () {
    gulp.watch('src/scss/**/*.scss' , gulp.parallel('scss'));
    gulp.watch('src/*.html', gulp.parallel('html'));
    gulp.watch('src/main/*.js', gulp.parallel('js'));
});

gulp.task('browser-sync',function () {
    sync.init({
        server:{
            baseDir:'src/'
        }
    })
});

gulp.task('default', gulp.parallel('browser-sync','watch'));